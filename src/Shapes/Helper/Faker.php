<?php

declare(strict_types=1);

namespace App\Shapes\Helper;

use App\Shapes\Entities\Circle;
use App\Shapes\Entities\Dot;
use App\Shapes\Entities\Rectangle;

class Faker
{
    /**
     * @param int $howMuch
     * @return array
     */
    public function fill(int $howMuch): array
    {
        $possibleShapes = ['Dot', 'Circle', 'Rectangle'];
        $shapes = [];

        for ($i = 0; $i < $howMuch; $i++) {
            $randomPossibleShape = $possibleShapes[rand(0, 2)];
            $shape = null;

            switch ($randomPossibleShape) {
                case 'Dot':
                    $shape = new Dot(rand(0, 50));
                    break;
                case 'Circle':
                    $shape = new Circle(rand(0, 20), ['x' => rand(0, 20), 'y' => rand(0, 20)]);
                    break;
                case 'Rectangle':
                    $shape = new Rectangle(rand(0, 30), rand(0, 15));
                    break;
                default;
            }

            $shapes[] = $shape;
        }

        return $shapes;
    }
}