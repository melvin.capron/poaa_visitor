<?php

declare(strict_types=1);

namespace App\Shapes\Entities;

use App\Shapes\Entities\Abstractions\Shape;
use App\Shapes\Export\Interfaces\VisitorInterface;

class Dot extends Shape
{
    /**
     * @var int
     */
    private $radius;

    /**
     * Dot constructor.
     * @param int $radius
     */
    public function __construct(int $radius)
    {
        parent::__construct();
        $this->radius = $radius;
    }

    /**
     * {@inheritDoc}
     */
    public function draw(): void
    {
        echo 'I\'m a ... dot?';
    }

    /**
     * {@inheritDoc}
     */
    public function accept(VisitorInterface $v): void
    {
        $v->visitDot($this);
    }

    /**
     * @return int
     */
    public function getRadius(): int
    {
        return $this->radius;
    }
}