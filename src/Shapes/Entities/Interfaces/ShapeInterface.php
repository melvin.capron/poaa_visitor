<?php

declare(strict_types=1);

namespace App\Shapes\Entities\Interfaces;

use App\Shapes\Export\Interfaces\VisitorInterface;

interface ShapeInterface
{
    /**
     * @return void
     */
    public function draw(): void;

    /**
     * @param VisitorInterface $v
     * @return void
     */
    public function accept(VisitorInterface $v): void;
}