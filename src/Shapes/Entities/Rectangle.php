<?php

declare(strict_types=1);

namespace App\Shapes\Entities;

use App\Shapes\Entities\Abstractions\Shape;
use App\Shapes\Export\Interfaces\VisitorInterface;

class Rectangle extends Shape
{
    /**
     * @var int
     */
    private $width;
    /**
     * @var int
     */
    private $height;

    /**
     * Rectangle constructor.
     * @param int $width
     * @param int $height
     */
    public function __construct(int $width, int $height)
    {
        parent::__construct();
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * {@inheritDoc}
     */
    public function draw(): void
    {
        echo 'I\'m a rectangle';
    }

    /**
     * {@inheritDoc}
     */
    public function accept(VisitorInterface $v): void
    {
        $v->visitRectangle($this);
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }
}