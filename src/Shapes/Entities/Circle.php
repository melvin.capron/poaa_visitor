<?php

declare(strict_types=1);

namespace App\Shapes\Entities;

use App\Shapes\Entities\Abstractions\Shape;
use App\Shapes\Export\Interfaces\VisitorInterface;

class Circle extends Shape
{
    /**
     * @var int
     */
    private $radius;
    /**
     * @var array
     */
    private $position;

    /**
     * Dot constructor.
     * @param int $radius
     * @param array $position
     */
    public function __construct(int $radius, array $position)
    {
        parent::__construct();
        $this->radius = $radius;
        $this->position = $position;
    }

    /**
     * {@inheritDoc}
     */
    public function draw(): void
    {
        echo 'I\'m a circle!';
    }

    /**
     * {@inheritDoc}
     */
    public function accept(VisitorInterface $v): void
    {
        $v->visitCircle($this);
    }

    /**
     * @return int
     */
    public function getRadius(): int
    {
        return $this->radius;
    }

    /**
     * @return array
     */
    public function getPosition(): array
    {
        return $this->position;
    }
}