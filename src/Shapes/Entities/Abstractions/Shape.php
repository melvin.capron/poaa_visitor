<?php

declare(strict_types=1);

namespace App\Shapes\Entities\Abstractions;

use App\Shapes\Entities\Interfaces\ShapeInterface;

abstract class Shape implements ShapeInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * Shape constructor.
     */
    public function __construct()
    {
        $this->id = rand();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}