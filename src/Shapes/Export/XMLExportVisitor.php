<?php

declare(strict_types=1);

namespace App\Shapes\Export;

use App\Shapes\Entities\Circle;
use App\Shapes\Entities\Dot;
use App\Shapes\Entities\Rectangle;
use App\Shapes\Export\Abstractions\AbstractExportVisitor;

class XMLExportVisitor extends AbstractExportVisitor
{
    /**
     * @inheritdoc
     */
    public function buildHeaders(): void
    {
        echo 'I\'m the XML export and I won`t build any headers \n';
    }

    /**
     * @inheritDoc
     */
    public function visitRectangle(Rectangle $rectangle): array
    {
        echo "Rectangle : [ID] = {$rectangle->getId()} | [Height] = {$rectangle->getHeight()} | [Width] = {$rectangle->getWidth()} \n";

        return [
            'id' => $rectangle->getId(),
            'height' => $rectangle->getHeight(),
            'width' => $rectangle->getWidth()
        ];
    }

    /**
     * @inheritDoc
     */
    public function visitDot(Dot $dot): array
    {
        echo "Dot : [ID] = {$dot->getId()} | [Radius] = {$dot->getRadius()} \n";

        return [
            'id' => $dot->getId(),
            'radius' => $dot->getRadius()
        ];
    }

    /**
     * @inheritDoc
     */
    public function visitCircle(Circle $circle): array
    {
        $stringifiedPosition = implode($circle->getPosition());
        echo "Circle : [ID] = {$circle->getId()} | [Radius] = {$circle->getRadius()} | [Position] = {$stringifiedPosition} \n";

        return [
            'id' => $circle->getId(),
            'radius' => $circle->getRadius(),
            'position' => $circle->getPosition()
        ];
    }
}