<?php

declare(strict_types=1);

namespace App\Shapes\Export\Interfaces;

use App\Shapes\Entities\Circle;
use App\Shapes\Entities\Dot;
use App\Shapes\Entities\Rectangle;

interface VisitorInterface
{
    /**
     * @return void
     */
    public function buildHeaders(): void;

    /**
     * @param Rectangle $rectangle
     * @return array
     */
    public function visitRectangle(Rectangle $rectangle): array;

    /**
     * @param Dot $dot
     * @return array
     */
    public function visitDot(Dot $dot): array;

    /**
     * @param Circle $circle
     * @return array
     */
    public function visitCircle(Circle $circle): array;
}