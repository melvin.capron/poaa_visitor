<?php

declare(strict_types=1);

namespace App\Shapes\Export;

use App\Shapes\Entities\Circle;
use App\Shapes\Entities\Dot;
use App\Shapes\Entities\Rectangle;
use App\Shapes\Export\Abstractions\AbstractExportVisitor;

class CsvExportVisitor extends AbstractExportVisitor
{
    /**
     * @inheritdoc
     */
    public function buildHeaders(): void
    {
        echo 'I\'m the CSV export and I\'m kind of break... \n';
    }

    /**
     * @inheritDoc
     */
    public function visitRectangle(Rectangle $rectangle): array
    {
        echo "fokdsfokds okos \n";

        return [
            'id' => $rectangle->getId(),
            'height' => $rectangle->getHeight(),
            'width' => $rectangle->getWidth()
        ];
    }

    /**
     * @inheritDoc
     */
    public function visitDot(Dot $dot): array
    {
        echo "aaaaaaaaaaaa \n";

        return [
            'id' => $dot->getId(),
            'radius' => $dot->getRadius()
        ];
    }

    /**
     * @inheritDoc
     */
    public function visitCircle(Circle $circle): array
    {
        echo "csqcsqcscccc cc \n";

        return [
            'id' => $circle->getId(),
            'radius' => $circle->getRadius(),
            'position' => $circle->getPosition()
        ];
    }
}