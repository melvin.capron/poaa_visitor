<?php

declare(strict_types=1);

namespace App\Shapes\Export\Abstractions;

use App\Shapes\Entities\Circle;
use App\Shapes\Entities\Dot;
use App\Shapes\Entities\Rectangle;
use App\Shapes\Export\Interfaces\VisitorInterface;

abstract class AbstractExportVisitor implements VisitorInterface
{
    /**
     * AbstractExportVisitor constructor.
     */
    public function __construct()
    {
        $this->buildHeaders();
    }

    /**
     * @inheritdoc
     */
    public abstract function buildHeaders(): void;

    /**
     * @inheritdoc
     */
    public abstract function visitRectangle(Rectangle $rectangle): array;

    /**
     * @inheritdoc
     */
    public abstract function visitDot(Dot $dot): array;

    /**
     * @inheritdoc
     */
    public abstract function visitCircle(Circle $circle): array;
}