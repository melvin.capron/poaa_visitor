<?php

use App\Shapes\Entities\Abstractions\Shape;
use App\Shapes\Entities\Circle;
use App\Shapes\Entities\Dot;
use App\Shapes\Entities\Rectangle;
use App\Shapes\Export\CsvExportVisitor;
use App\Shapes\Export\XMLExportVisitor;
use App\Shapes\Helper\Faker;

ini_set('display_errors', 1);

require('vendor/autoload.php');

$xmlExport = new XMLExportVisitor();

$shapes = (new Faker())->fill(rand(0, 8));

foreach ($shapes as $shape) {
    /** @var Shape $shape */
    $shape->accept($xmlExport);
}

echo "--- NEXT EXPORT --- \n";
$csvExport = new CsvExportVisitor();

foreach ($shapes as $shape) {
    $shape->accept($csvExport);
}